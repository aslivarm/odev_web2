﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace firstMVC.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        // GET: Forms
        public ActionResult Forms()
        {
            return View();
        }

        // Get: TopDown
        public ActionResult TopDown()
        {
            return View();
        }
        // Get: TopFavourite
        public ActionResult TopFavourite()
        {
            return View();
        }

    }
}